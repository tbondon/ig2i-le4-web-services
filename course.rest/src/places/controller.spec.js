const request = require("supertest");
const assert = require("assert");
const App = require("../app");
const PlaceData = require("./data");
const Place = require("./controller");

describe("Places/controller", () => {
	it("GET /api/version should respond a http 200 OK", () => {
		const app = new App(new Place(new PlaceData())).app;
		return request(app)
			.get("/api/version")
			.expect("Content-Type", /json/)
			.expect(200)
			.then(response => {
				expect(response.body.version).toBe("1.0.0");
			});
	});

	it("GET /api/hello should respond a http 404 KO", () => {
		const app = new App(new Place(new PlaceData())).app;
		return request(app)
			.get("/api/hello")
			.expect("Content-Type", /json/)
			.expect(404);
	});

	it("GET /api/places/2 should respond a http 200 OK", () => {
		const app = new App(new Place(new PlaceData())).app;
		return request(app)
			.get("/api/places/2")
			.expect("Content-Type", /json/)
			.expect(200)
			.then(response => {
				expect(response.body.author).toBe("Louis");
			});
	});

	it("GET /api/places/youhou should respond a http 404", () => {
		const app = new App(new Place(new PlaceData())).app;
		return request(app)
			.get("/api/places/youhou")
			.expect("Content-Type", /json/)
			.expect(404)
			.expect(response => {
				expect(response.body.key).toBe("entity.not.found");
			});
	});

	it("GET /api/places should respond 3", () => {
		const placeData = new PlaceData();
		const app = new App(new Place(placeData)).app;
		return request(app)
			.get("/api/places")
			.expect("Content-Type", /json/)
			.expect(200)
			.expect(async response => {
				expect(response.body.length).toBe(
					(await placeData.getPlacesAsync()).length
				);
			});
	});

	it("POST /api/places should respond a http 201 OK with no image", () => {
		var newPlace = {
			name: "Londre",
			author: "Patrick",
			review: 2
		};
		const app = new App(new Place(new PlaceData())).app;
		return request(app)
			.post("/api/places")
			.send(newPlace)
			.expect("Location", /places/)
			.expect(201);
	});

	it("POST /api/places should respond a http 201 OK with an image", () => {
		var newPlace = {
			name: "Londre",
			author: "Patrick",
			review: 2,
			image: {
				url:
					"https://www.bworld.fr/api/file/get/c27e39ee-7ba9-46f8-aa7c-9e334c72a96c/d9d0634b-b1a0-42bd-843d-d3bc3cf7d842/ImageThumb/bworld-2016-v3.png",
				title: "bworld place"
			}
		};
		const app = new App(new Place(new PlaceData())).app;
		return request(app)
			.post("/api/places")
			.send(newPlace)
			.expect("Location", /places/)
			.expect(201);
	});

	it("POST /api/places should respond a http 400 KO", () => {
		var newPlace = {
			name: "",
			author: "Pat",
			review: 2
		};
		const app = new App(new Place(new PlaceData())).app;
		return request(app)
			.post("/api/places")
			.send(newPlace)
			.expect("Content-Type", /json/)
			.expect(400);
	});

	it("POST /api/places should respond a http 400 KO", () => {
		const app = new App(new Place(new PlaceData())).app;
		var newPlace = {
			name: "Londre &",
			author:
				"Patrickmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm",
			review: 2,
			image: {
				url:
					"https://www.bworld.fr/api/file/get/c27e39ee-7ba9-46f8-aa7c-9e334c72a96c/d9d0634b-b1a0-42bd-843d-d3bc3cf7d842/ImageThumb/bworld-2016-v3.png",
				title: ""
			}
		};
		return request(app)
			.post("/api/places")
			.send(newPlace)
			.expect("Content-Type", /json/)
			.expect(400);
	});

	it("DELETE /api/places/2 should respond a http 204 OK", () => {
		const app = new App(new Place(new PlaceData())).app;
		return request(app)
			.delete("/api/places/2")
			.expect("Location", /places/)
			.expect(204);
	});

	it("DELETE /api/places/9 should respond a http 404 KO", () => {
		const app = new App(new Place(new PlaceData())).app;
		return request(app)
			.delete("/api/places/9")
			.expect("Location", /places/)
			.expect(404);
	});

	it("PUT /api/places/3 should respond a http 204 OK", () => {
		const app = new App(new Place(new PlaceData())).app;
		let newPlace = {
			name: "Lesigny",
			author: "Timothee",
			review: 77,
			image: null
		};
		return request(app)
			.put("/api/places/3")
			.send(newPlace)
			.expect(204);
	});

	it("PATCH /api/places/1 should respond a http 204 OK", () => {
		const app = new App(new Place(new PlaceData())).app;
		let newPlace = {
			name: "Paris"
		};
		return request(app)
			.patch("/api/places/1")
			.send(newPlace)
			.expect(204);
	});

	it("PATCH /api/places/1 should respond a http 204 OK", () => {
		const app = new App(new Place(new PlaceData())).app;
		let newPlace = [
			{ op: "replace", path: "/name", value: "Saint-brieuc" },
			{
				op: "replace",
				path: "/author",
				value: "Robert"
			}
		];
		return request(app)
			.patch("/api/places/1")
			.set("Content-Type", "application/merge-patch+json")
			.send(newPlace)
			.expect(200)
			.expect(response => {
				expect(response.body).toStrictEqual({
					author: "Robert",
					id: "1",
					image: null,
					name: "Saint-brieuc",
					review: 2
				});
			});
	});

	it("GET /api/places?name=le should respond a http 200 OK", () => {
		const app = new App(new Place(new PlaceData())).app;
		return request(app)
			.get("/api/places?name=Le")
			.expect(200)
			.expect(response => {
				expect(response.body).toStrictEqual([
					{
						author: "Timothee",
						id: "3",
						image: null,
						name: "Lesigny",
						review: 77
					}
				]);
			});
	});
});
