const validation = require("mw.validation");
const { applyPatch } = require("fast-json-patch");

class Places {
	constructor(data) {
		this.data = data;
	}
	configure(app) {
		const data = this.data;

		app.get("/api/places/:id", function (request, response) {
			response.setHeader("Location", "places");

			let id = request.params.id;

			return data.getPlaceAsync(id).then(function (place) {
				if (place !== undefined) {
					response.status(200).json(place);
					return;
				}
				response.status(404).json({ key: "entity.not.found" });
			});
		});

		app.get("/api/places", function (request, response) {
			response.setHeader("Location", "places");
			response.setHeader(
				"Access-Control-Allow-Origin",
				"http://localhost:3000"
			);
			response.setHeader("Access-Control-Allow-Methods", "GET, POST");
			response.setHeader(
				"Access-Control-Allow-Headers",
				"Content-Type, my-header-custom"
			);
			response.setHeader("Cache-Control", "max-age=15, public");

			return data.getPlacesAsync().then(function (places) {
				for (let i = 0; i < places.length; i++) {
					if (
						(request.query["name"] &&
							!places[i].name.includes(request.query["name"])) ||
						(request.query["author"] &&
							!places[i].author.includes(request.query["author"]))
					) {
						places.splice(i, 1);
						i--;
					}
				}

				response.status(200).json({ places });
				return;
			});
		});

		app.post("/api/places", function (request, response) {
			response.setHeader("Location", "places");
			response.setHeader(
				"Access-Control-Allow-Origin",
				"http://localhost:3000"
			);
			response.setHeader("Access-Control-Allow-Methods", "GET, POST");
			response.setHeader(
				"Access-Control-Allow-Headers",
				"Content-Type, my-header-custom"
			);

			let place = request.body;

			let onlyIf = function () {
				return !!(place.image && place.image.url);
			};

			let rules1 = {
				name: [
					"required",
					{ minLength: { minLength: 3 } },
					{ maxLength: { maxLength: 100 } },
					{ pattern: { regex: /^[a-zA-Z -]*$/ } },
				],
				author: [
					"required",
					{ minLength: { minLength: 3 } },
					{ maxLength: { maxLength: 100 } },
					{ pattern: { regex: /^[a-zA-Z -]*$/ } },
				],
				review: ["required", "digit"],
			};

			let result = validation.objectValidation.validateModel(
				place,
				rules1
			);

			if (!result.success) {
				response.status(400).json();
				return;
			}

			if (place.image) {
				let rules2 = {
					url: ["url"],
					title: [
						{
							required: {
								onlyIf: onlyIf,
								message: "Field Image title is required",
							},
						},
						{ minLength: { minLength: 3 } },
						{ maxLength: { maxLength: 100 } },
						{ pattern: { regex: /^[a-zA-Z -]*$/ } },
					],
				};

				let result = validation.objectValidation.validateModel(
					place.image,
					rules2
				);

				if (!result.success) {
					response.status(400).json();
					return;
				}
			}

			return data.savePlaceAsync(place).then((id) => {
				response.status(201).json(id);
				return;
			});
		});

		app.delete("/api/places/:id", function (request, response) {
			response.setHeader("Location", "places");

			let id = request.params.id;

			return data.deletePlaceAsync(id).then((deleted) => {
				if (deleted) {
					response.status(204).json(id);
					return;
				}
				response.status(404).json();
				return;
			});
		});

		app.put("/api/places/:id", function (request, response) {
			response.setHeader("Location", "places");

			let id = request.params.id;
			let place = request.body;

			let onlyIf = function () {
				return !!(place.image && place.image.url);
			};

			let rules1 = {
				name: [
					"required",
					{ minLength: { minLength: 3 } },
					{ maxLength: { maxLength: 100 } },
					{ pattern: { regex: /^[a-zA-Z -]*$/ } },
				],
				author: [
					"required",
					{ minLength: { minLength: 3 } },
					{ maxLength: { maxLength: 100 } },
					{ pattern: { regex: /^[a-zA-Z -]*$/ } },
				],
				review: ["required", "digit"],
			};

			let result = validation.objectValidation.validateModel(
				place,
				rules1
			);

			if (!result.success) {
				response.status(400).json();
				return;
			}

			if (place.image) {
				let rules2 = {
					url: ["url"],
					title: [
						{
							required: {
								onlyIf: onlyIf,
								message: "Field Image title is required",
							},
						},
						{ minLength: { minLength: 3 } },
						{ maxLength: { maxLength: 100 } },
						{ pattern: { regex: /^[a-zA-Z -]*$/ } },
					],
				};

				result = validation.objectValidation.validateModel(
					place.image,
					rules2
				);

				if (!result.success) {
					response.status(400).json();
					return;
				}
			}

			place.id = id;
			return data.getPlaceAsync(id).then((place2) => {
				if (place2 === undefined) {
					return data.savePlaceAsync(place).then((id) => {
						response.status(201).json(id);
						return;
					});
				}

				return data.savePlaceAsync(place).then((id) => {
					response.status(204).json();
					return;
				});
			});
		});

		app.patch("/api/places/:id", (request, response) => {
			response.setHeader("Location", "places");

			let id = request.params.id;
			let place = request.body;

			if (
				request.headers["content-type"].includes(
					"application/merge-patch+json"
				)
			) {
				return data.getPlaceAsync(id).then((place2) => {
					if (place2 === undefined) {
						response.status(404).json(id);
						return;
					}

					place2 = applyPatch(place2, place).newDocument;
					return data.savePlaceAsync(place2).then((id) => {
						response.status(200).json(place2);
						return;
					});
				});
			}

			let onlyIf = function () {
				return !!(place.image && place.image.url);
			};

			let rules1 = {
				name: [
					"required",
					{ minLength: { minLength: 3 } },
					{ maxLength: { maxLength: 100 } },
					{ pattern: { regex: /^[a-zA-Z -]*$/ } },
				],
				author: [
					"required",
					{ minLength: { minLength: 3 } },
					{ maxLength: { maxLength: 100 } },
					{ pattern: { regex: /^[a-zA-Z -]*$/ } },
				],
				review: ["required", "digit"],
			};

			let result = validation.objectValidation.validateModel(
				place,
				rules1
			);

			if (!result.success) {
				response.status(400).json();
				return;
			}

			if (place.image) {
				let rules2 = {
					url: ["url"],
					title: [
						{
							required: {
								onlyIf: onlyIf,
								message: "Field Image title is required",
							},
						},
						{ minLength: { minLength: 3 } },
						{ maxLength: { maxLength: 100 } },
						{ pattern: { regex: /^[a-zA-Z -]*$/ } },
					],
				};

				result = validation.objectValidation.validateModel(
					place.image,
					rules2
				);

				if (!result.success) {
					response.status(400).json();
					return;
				}
			}

			place.id = id;
			return data.getPlaceAsync(id).then((place2) => {
				if (place2 === undefined) {
					response.status(404).json(id);
					return;
				}

				Object.assign(place2, place);
				return data.savePlaceAsync(place2).then((id) => {
					response.status(204).json();
					return;
				});
			});
		});

		app.options("/api/places", function (request, response) {
			response.setHeader(
				"Access-Control-Allow-Origin",
				"http://localhost:3000"
			);
			response.setHeader("Access-Control-Allow-Methods", "GET, POST");
			response.setHeader(
				"Access-Control-Allow-Headers",
				"Content-Type, my-header-custom"
			);
			response.setHeader("Cache-Control", "max-age=30, public");
			response.json();
		});
	}
}
module.exports = Places;
